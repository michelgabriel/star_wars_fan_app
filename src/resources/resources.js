import React, { Component } from 'react';
import { Link } from 'react-router-dom';


// Get individual Resource
class Resources extends Component{
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            resource: {},
            path: "",
            page: 1
        };
    }

    componentWillReceiveProps(props) {
        if(props && props.match){
            this.setState({
                path: props.match.params.path,
                page:1
            }, this.fetchData);
            console.log("receive")
        }
    }

    componentDidMount(props){
        if(this.props && this.props.match){
            this.setState({
                path: this.props.match.params.path
            }, this.fetchData);
            
            console.log("mount")
        }
    }

    // FetchData based on url
    fetchData(){
        let url;
        if(this.state.path){
            url = `https://swapi.co/api/${this.state.path}/?page=${this.state.page}`
        }
        fetch(url)
        .then(res => res.json())
            .then(result => {
                this.setState({
                    isLoaded: true,
                    resource: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            })
    }

    getID(url){
        let res = url.split(`https://swapi.co/api/`);
        let id = res[1].substring(res[1].indexOf("/")+1, res[1].length-1)
        return id;
    }

    handleNextPage(){
        let pagenr = this.state.page
        this.setState({
            page: pagenr+1,
            isLoaded:false
        },this.fetchData)
    }

    render(){
        const { error, isLoaded, resource } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className="col-md-3">Loading...</div>;
        } else {
            if(this.state.resource.results){
                return (
                    <div className="col-md-3">
                        <h2>{this.state.path.charAt(0).toUpperCase() + this.state.path.slice(1)}</h2>
                        <ul className="list-group">
                            {this.state.resource.results.map(result => (
                                // Create links to a detail view of an item
                                <Link className="list-group-item list-group-item-action" to={`/${this.state.path}/${this.getID(result.url)}`} key={result.title || result.name}>{result.title || result.name}</Link>                                              
                            ))}
                        </ul><br/>
                        <button type="button" className="btn btn-outline-light" onClick={this.handleNextPage.bind(this)}>Next Page</button>
                    </div>
                )
            } else {
                return (
                    <div className="col-md-3">
                        <p>There are no more results for this resource</p>
                    </div>
                )
            }
        }
    }
}
export default Resources;