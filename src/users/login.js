import React, { Component } from 'react';

// Login
class Login extends Component{
    constructor(){
        super();
        let result = require('./login.json');
        this.state = {
            username: "",
            password: "",
            auth: result
        };
    }

    // Change state of username on input
    changeUsername(event){
        this.setState({
            username: event.target.value
        });
    }

    // Change state of password on input
    changePassword(event){
        this.setState({
            password: event.target.value
        });
    }

    // Check if username and password are correct
    handleLogin(event){
        event.preventDefault();
        if(this.state.username === this.state.auth.username && this.state.password === this.state.auth.password){
            this.props.login();
        } else {
            return (
                alert("This username and password combination doesn't exist! Try again.")                    
            )
        }       
    }
    
    // Login form
    render(){
            return (
                <div className="d-flex justify-content-center">
                    <form className="form" onSubmit={this.handleLogin.bind(this)}>
                        <h2 className="">Please log in</h2>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Username" value={this.state.username} onChange={this.changeUsername.bind(this)} autoFocus/>
                        </div>
                        <div className="form-group">
                            <input type="password" className="form-control" placeholder="Password" value={this.state.password} onChange={this.changePassword.bind(this)} />
                        </div>                            
                        <button className="btn btn-outline-light btn-block" type="submit">Login</button>
                    </form>
                </div>
            )
        }
    }
export default Login;