import React, { Component } from 'react';

import FilmItem from './items/filmitem';
import PeopleItem from './items/peopleitem';
import PlanetItem from './items/planetitem';
import SpeciesItem from './items/speciesitem.js';
import StarshipItem from './items/starshipitem.js';
import VehicleItem from './items/vehicleitem.js';


// Searchfield
class Search extends Component{
    constructor(){
        super();
        this.state = {
            search: "",
            load: false
        }
    }

    handleChange(event){
        this.setState({
            search: event.target.value,
            load:false
        });
    }
    
    handleSearch(event){
        this.setState({
            load:true
        })
        event.preventDefault();
    }

    render(){
            return (
                    <div className="row">
                        <form className="col-md-3" onSubmit={this.handleSearch.bind(this)}>
                            <div className="form-group">
                                <input className="form-control mt-2" type="search" value={this.state.search} onChange={this.handleChange.bind(this)} placeholder="Enter search term..." autoFocus/>
                            </div>
                            <button type="submit" className="btn btn-outline-light mb-2"> Search </button>
                        </form>
                        <div className="col-md-12">
                            {/* Check if search was submitted and load all results */}
                            {this.state.load ? 
                                <div className="row">
                                    <FilmItem search={this.state.search}/>
                                    <PeopleItem search={this.state.search}/>
                                    <PlanetItem search={this.state.search}/>                                
                                    <SpeciesItem search={this.state.search}/>
                                    <StarshipItem search={this.state.search}/>
                                    <VehicleItem search={this.state.search}/>
                                </div> 
                            : null}
                        </div>
                    </div>
                
            )
        }
    }
export default Search;