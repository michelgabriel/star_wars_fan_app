import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import FilmItem from './filmitem.js';
import PeopleItem from './peopleitem.js';
import PlanetItem from './planetitem.js';
import SpeciesItem from './speciesitem.js';
import StarshipItem from './starshipitem.js';
import VehicleItem from './vehicleitem.js';

// Routes to a detail view of the items
class Items extends Component{
    render(){
        return (
            <div className="col-md-3">
                <Route path="/films/:id" component={FilmItem} />
                <Route path="/people/:id" component={PeopleItem} />
                <Route path="/planets/:id" component={PlanetItem} />
                <Route path="/species/:id" component={SpeciesItem} />
                <Route path="/starships/:id" component={StarshipItem} />
                <Route path="/vehicles/:id" component={VehicleItem} />
            </div>
        )
    }
}
export default Items;