import React, { Component } from 'react';


// Details of people
class PeopleItem extends Component{
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            item: {},
            id: "",
            search: ""
        };
    }
    
    componentWillReceiveProps(props) {
        if(props && props.search){
            this.setState({
                search: props.search
            }, this.fetchData);
        } else if(props && props.match){
            this.setState({
                id: props.match.params.id
            }, this.fetchData);
        }
    }
    
    componentDidMount(props){
        if(this.props.search){
            this.setState({
                search: this.props.search
            }, this.fetchData);
        } else if(this.props.match){
            this.setState({
                id: this.props.match.params.id
            }, this.fetchData);
        }
    }

    fetchData() {
        let url;
        if(this.state.search){
            url = `https://swapi.co/api/people/?search=${this.state.search}`
        } else if(this.state.id){
            url = `https://swapi.co/api/people/${this.state.id}`
        }
        
        fetch(url)
        .then(res => res.json())
            .then(result => {
                this.setState({
                    isLoaded: true,
                    item: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    render(){
        const { error, isLoaded, item } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className="col-md-3"></div>;
        } else {
            // Check if component is loaded through the searchfield or the detail view
            if(this.state.item && this.state.item.results) {
                if(this.state.item.count == 0)
                    return null
                return(
                        <div className="col-md-3">
                            <h2>People</h2>
                            {this.state.item.results.map((result) => (
                                <table className="table" key={result.name}>
                                    <thead>
                                        <tr>
                                            <th colSpan="2">{result.name}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                            
                                            <th>Height</th>
                                            <td>{result.height}</td>
                                        </tr>
                                        <tr>
                                            <th>Weight</th>
                                            <td>{result.mass}</td>
                                        </tr>
                                        <tr>                            
                                            <th>Birth year</th>
                                            <td>{result.birth_year}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            ))}
                        </div>
                    )
            } else {
                return (
                    <table className="table table-res" >
                        <thead>
                            <tr>
                                <th colSpan="2">{this.state.item.name}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                            
                                <th>Height</th>
                                <td>{this.state.item.height}</td>
                            </tr>
                            <tr>
                                <th>Weight</th>
                                <td>{this.state.item.mass}</td>
                            </tr>
                            <tr>                            
                                <th>Birth year</th>
                                <td>{this.state.item.birth_year}</td>
                            </tr>
                        </tbody>
                    </table>
                )
            }
        }
    }
}
export default PeopleItem;