import React, { Component } from 'react';


// Details of a planet
class PlanetItem extends Component{
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            item: {},
            id: "",
            search: ""
        };
    }
    
    componentWillReceiveProps(props) {
        if(props && props.search){
            this.setState({
                search: props.search
            }, this.fetchData);
        } else if(props && props.match){
            this.setState({
                id: props.match.params.id
            }, this.fetchData);
        }
    }
    
    componentDidMount(props){
        if(this.props.search){
            this.setState({
                search: this.props.search
            }, this.fetchData);
        } else if(this.props.match){
            this.setState({
                id: this.props.match.params.id
            }, this.fetchData);
        }
    }

    fetchData() {
        let url;
        if(this.state.search){
            url = `https://swapi.co/api/planets/?search=${this.state.search}`
        } else if(this.state.id){
            url = `https://swapi.co/api/planets/${this.state.id}`
        }
        
        fetch(url)
        .then(res => res.json())
            .then(result => {
                this.setState({
                    isLoaded: true,
                    item: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    render(){
        const { error, isLoaded, item } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className="col-md-3"></div>;
        } else {
            // Check if component is loaded through the searchfield or the detail view
            if(this.state.item && this.state.item.results) {
                if(this.state.item.count == 0)
                    return null
                return(
                        <div className="col-md-3">
                            <h2>Planets</h2>
                            {this.state.item.results.map((result) => (
                                <table className="table" key={result.name}>
                                    <thead>
                                        <tr>
                                            <th colSpan="2">{result.name}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                            
                                            <th>Terrain</th>
                                            <td>{result.terrain}</td>
                                        </tr>
                                        <tr>
                                            <th>Gravity</th>
                                            <td>{result.gravity}</td>
                                        </tr>
                                        <tr>                            
                                            <th>Population</th>
                                            <td>{result.population}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            ))}
                        </div>
                    )
            } else {
                return (
                    <table className="table table-res" >
                        <thead>
                            <tr>
                                <th colSpan="2">{this.state.item.name}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                            
                                <th>Terrain</th>
                                <td>{this.state.item.terrain}</td>
                            </tr>
                            <tr>
                                <th>Gravity</th>
                                <td>{this.state.item.gravity}</td>
                            </tr>
                            <tr>                            
                                <th>Population</th>
                                <td>{this.state.item.population}</td>
                            </tr>
                        </tbody>
                    </table>
                )
            }
        }
    }
}
export default PlanetItem;