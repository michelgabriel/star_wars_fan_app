import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Items from './items/items';
import Search from './search';
import Resources from './resources';

// Topbar Navigation
class Nav extends Component{
    constructor(){
        super();
    }
    
    render(){
            return (
                <div className="col-md-12">
                    <ul className="nav">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">Search</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/films">Film</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/people">People</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/planets">Planets</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/species">Species</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/starships">Starships</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/vehicles">Vehicles</Link>
                        </li>
                    </ul>
                    {/* Route to Searchfield */}
                    <Route className="" exact path="/" component={Search} />
                    <div className="row">
                        {/* Route to individual Resource */}
                        <Route className="" path="/:path" component={Resources} />
                        <Items />
                    </div>
                </div>
            )
        }
    }
export default Nav;