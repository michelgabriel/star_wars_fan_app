import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import './_css/App.css';
import Nav from './resources/nav';
import Items from './resources/items/items';
import Login from './users/login';


class App extends Component{
  constructor(props){
    super(props);
    this.handler = this.handler.bind(this);
    this.state = {
      loggedin: false,
    };
  }

  handler() {
    this.setState({
      loggedin: true
    });
  }

  render(){
    // Check if user is logged in otherwise show login screen
    if(!this.state.loggedin){
      return (
        <Login login={this.handler}/>
      )
  } else{
      return (
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <h1>Star Wars Fan App</h1>
            </div>
          </div>
          <Router>
            <div className="row">
              <Nav />
            </div>
          </Router>
        </div>
      )
    }
  }
}

export default App;
