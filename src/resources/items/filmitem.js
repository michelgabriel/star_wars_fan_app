import React, { Component } from 'react';


// Details of a film
class FilmItem extends Component{
    constructor(props){
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            item: {},
            id: "",
            search: ""
        };
    }

    componentWillReceiveProps(props) {
        if(props && props.search){
            this.setState({
                search: props.search
            }, this.fetchData);
        } else if(props && props.match){
            this.setState({
                id: props.match.params.id
            }, this.fetchData);
        }
    }
    
    componentDidMount(props){
        if(this.props.search){
            this.setState({
                search: this.props.search
            }, this.fetchData);
        } else if(this.props.match){
            this.setState({
                id: this.props.match.params.id
            }, this.fetchData);
        }
    }
    
    fetchData() {
        let url;
        if(this.state.search){
            url = `https://swapi.co/api/films/?search=${this.state.search}`
        } else if(this.state.id){
            url = `https://swapi.co/api/films/${this.state.id}`
        }
            
        fetch(url)
        .then(res => res.json())
            .then(result => {
                this.setState({
                    isLoaded: true,
                    item: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }
    
    render(){
        const { error, isLoaded, item } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className="col-md-3">Loading...</div>;
        } else {
            // Check if component is loaded through the searchfield or the detail view
            if(this.state.item && this.state.item.results) {
                if(this.state.item.count == 0)
                    return null
                return(
                        <div className="col-md-3">
                            <h2>Films</h2>
                            {this.state.item.results.map((result) => (
                                <table className="table" key={result.title}>
                                    <thead>
                                        <tr>
                                            <th colSpan="2">{result.title}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                            
                                            <th>Episode</th>
                                            <td>{result.episode_id}</td>
                                        </tr>
                                        <tr>
                                            <th>Released</th>
                                            <td>{result.release_date}</td>
                                        </tr>
                                        <tr>                            
                                            <th>Directed by</th>
                                            <td>{result.director}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            ))}
                        </div>
                    )
            } else {
                return (
                    <table className="table table-res">
                        <thead>
                            <tr>
                                <th colSpan="2">{this.state.item.title}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                            
                                <th>Episode</th>
                                <td>{this.state.item.episode_id}</td>
                            </tr>
                            <tr>
                                <th>Released</th>
                                <td>{this.state.item.release_date}</td>
                            </tr>
                            <tr>                            
                                <th>Directed by</th>
                                <td>{this.state.item.director}</td>
                            </tr>
                        </tbody>
                    </table>
                )
            }
        }
    }
}
export default FilmItem;